#!/usr/bin/env python
# -*- coding: utf-8 -*-
try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin

import requests


class Merchant(object):
    def __init__(self, guid, main_password, second_password=None):
        self.guid = guid
        self.main_password = main_password
        self.second_password = second_password

    @property
    def base_merchant_url(self):
        return 'https://blockchain.info/merchant/{guid}/'.format(self.guid)

    def get_params(self, **kwargs):
        base = {'main_password': self.main_password}
        if self.second_password:
            base['second_password'] = self.second_password
        base.update(**kwargs)
        return base

    def post(self, url, params):
        return requests.post(url, parmas=params).json()

    def payment(self, to, amount, _from, shared=None, fee=None, note=None):
        params = self.get_params(**{'to': to, 'amount': amount, 'from': _from, 'shared': shared,
                                    'fee': fee, 'note': note})
        url = urljoin(self.base_merchant_url, '/payment')
        return self.post(url, params)

    def sendmany(self, recipients, amount, _from, shared=None, fee=None, note=None):
        params = self.get_params(**{'recipients': recipients, 'amount': amount, 'from': _from,
                                    'shared': shared, 'fee': fee, 'note': note})
        url = urljoin(self.base_merchant_url, '/sendmany')
        return self.post(url, params)

    def balance(self):
        params = self.get_params()
        url = urljoin(self.base_merchant_url, '/balance')
        return self.post(url, params)

    def list(self):
        params = self.get_params()
        url = urljoin(self.base_merchant_url, '/list')
        return self.post(url, params)

    def address_balance(self, address, confirmations):
        params = self.get_params(**{'address': address, 'confirmations': confirmations})
        url = urljoin(self.base_merchant_url, '/address_balance')
        return self.post(url, params)

    def new_address(self, label=None):
        params = self.get_params(**{'label': label})
        url = urljoin(self.base_merchant_url, '/new_address')
        return self.post(url, params)

    def archive_address(self, address):
        params = self.get_params(**{'address': address})
        url = urljoin(self.base_merchant_url, '/archive_address')
        return self.post(url, params)

    def unarchive_address(self, address):
        params = self.get_params(**{'address': address})
        url = urljoin(self.base_merchant_url, '/unarchive_address')
        return self.post(url, params)

    def auto_consolidate(self, days):
        params = self.get_params(**{'days': days})
        url = urljoin(self.base_merchant_url, '/auto_consolidate')
        return self.post(url, params)


class Blockchain(object):
    ### Blockchain Data API ###

    @classmethod
    def single_block(cls, value):
        """

        :param value: Block index or hash
        :return: JSON blob
        """
        url = 'http://blockchain.info/rawblock/{value}'.format(value=value)
        return requests.get(url).json()

    @classmethod
    def single_transaction(cls, value):
        """

        :param value: Transaction index or hash
        :return: JSON blob
        """
        url = 'http://blockchain.info/rawtx/{value}'.format(value=value)
        return requests.get(url).json()

    @classmethod
    def chart_data(cls, chart_type):
        """

        :param chart_type:
        :return:
        """
        url = 'http://blockchain.info/charts/{chart_type}?format=json'.format(chart_type=chart_type)
        return requests.get(url).json()

    @classmethod
    def block_height(cls, block_height):
        """

        :param block_height:
        :return:
        """
        url = 'http://blockchain.info/block-height/{block_height}?format=json'.format(block_height=block_height)
        return requests.get(url).json()

    @classmethod
    def single_address(cls, address, limit=50, offset=0):
        """

        :param address:
        :param limit:
        :param offset:
        :return:
        """
        url = 'http://blockchain.info/rawaddr/{address}?limit={limit}&offset={offset}'.format(address=address,
                                                                                              limit=limit,
                                                                                              offset=offset)
        return requests.get(url).json()

    @classmethod
    def multi_address(cls, addresses):
        """

        :param addresses:
        :return:
        """
        url = 'http://blockchain.info/multiaddr?active={addresses}'.format('|'.join(addresses))
        return requests.get(url).json()

    @classmethod
    def unspent_outputs(cls, addresses):
        """

        :param addresses:
        :return:
        """
        url = 'http://blockchain.info/unspent?active={addresses}'.format('|'.join(addresses))
        return requests.get(url).json()

    @classmethod
    def latest_block(cls):
        """

        :return:
        """
        url = 'http://blockchain.info/latestblock'
        return requests.get(url).json()

    @classmethod
    def unconfirmed_transactions(cls):
        """

        :return:
        """
        url = 'http://blockchain.info/unconfirmed-transactions?format=json'
        return requests.get(url).json()

    @classmethod
    def blocks(cls, value):
        """

        :param value: Time in milliseconds or pool name
        :return:
        """
        url = 'http://blockchain.info/blocks/{value}?format=json'.format(value=value)
        return requests.get(url).json()

    @classmethod
    def inventory_data(cls, hash):
        """

        :param hash:
        :return:
        """
        url = 'http://blockchain.info/inv/{hash}?format=json'.format(hash=hash)
        return requests.get(url).json()

    ### Market Data ###

    @classmethod
    def ticker(cls):
        """

        :return:
        """
        url = 'http://blockchain.info/ticker'
        return requests.get(url).json()

    @classmethod
    def to_btc(cls, currency, value):
        """

        :param currency:
        :param value:
        :return:
        """
        url = 'http://blockchain.info/tobtc?currency={currency}&value={value}'.format(currency=currency, value=value)
        return requests.get(url).json()

    @classmethod
    def charts(cls):
        """

        :return:
        """
        url = ' http://blockchain.info/charts/$chart-type?format=json'
        return requests.get(url).json()

    @classmethod
    def stats(cls):
        """

        :return:
        """
        url = 'http://blockchain.info/stats?format=json'
        return requests.get(url).json()
