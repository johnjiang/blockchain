#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='blockchain',
    version='0.1.0',
    description='Python blockchain.info api wrapper',
    long_description=readme + '\n\n' + history,
    author='John Jiang',
    author_email='johnjiang101@gmail.com',
    url='https://bitbucket.org/johnjiang/blockchain',
    packages=[
        'blockchain',
    ],
    package_dir={'blockchain': 'blockchain'},
    include_package_data=True,
    install_requires=[
        'requests'
    ],
    license='BSD',
    zip_safe=False,
    keywords='blockchain',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
    ],
    test_suite='tests',
)