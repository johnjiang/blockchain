============
Installation
============

At the command line::

    $ easy_install blockchain

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv blockchain
    $ pip install blockchain